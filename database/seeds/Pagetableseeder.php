<?php

use Illuminate\Database\Seeder;

class Pagetableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page')->insert([
             'name' => 'Dashboard'
        ]);

        DB::table('page')->insert([
            'name' => 'Home'
        ]);

        DB::table('page')->insert([
            'name' => 'Users'
        ]);

        DB::table('page')->insert([
            'name' => 'Posts'
        ]);

        DB::table('page')->insert([
            'name' => 'AddUser'
        ]);

        DB::table('page')->insert([
            'name' => 'AddPost'
        ]);

        DB::table('page')->insert([
            'name' => 'EditUser'
        ]);

        DB::table('page')->insert([
            'name' => 'EditPost'
        ]);

        DB::table('page')->insert([
            'name' => 'BlockedUsers'
        ]);
    }
}
