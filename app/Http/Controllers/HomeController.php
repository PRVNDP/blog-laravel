<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Helper\JsonApiResponse;
use App\Posts;
use App\User;
use App\Roles;
use Validator;

class Home extends Controller
{
    public function index(){
        $data = Posts::paginate(10);
        return JsonApiResponse::success('Posts', [$data]);
    }

    public function permissions(Request $request){
        // // $permission = Permission::create(['name' => 'viewPosts']);
        // // $permission =  Permission::create(['name'=>'viewUsers']);
        // // $permission = Permission::findByName('writePost');
        // $permission = Permission::findById(6);
        // $role = Role::findById(3);
        // $assign = Auth::user()->assignRole($role);
        // // // $revoke = $role->revokePermissionTo($permission);
        // // $assign = $role->givePermissionTo($permission);
        // // // // $assign1 = Auth::user()->getAllPermissions();
        // return $assign;
    }


}
