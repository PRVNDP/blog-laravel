<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Helper\JsonApiResponse;
use App\Posts;
use App\User;
use App\Roles;
use Validator;

class PostController extends Controller
{
    public function Index(Request $request){
        $sortBy = $request->sortBy;
        $order = $request->sortDirection;
        $search = $request->search;
        $users = new Posts();
        if(!Auth::user()->hasAnyPermission(['viewPosts'])){
              return JsonApiResponse::denyPermission();
        }
        if(isset($search)){
            $users = $users->where([['title', 'LIKE',"%{$search}%"]])
                           ->orWhere([['content', 'LIKE', "%{$search}%"]]);
        }
        if(isset($sortBy) && isset($order)){
            $users = $users->orderBy($sortBy, $order);
        }
        $data = $users->paginate(10);
        return JsonApiResponse::success('Posts',[$data]);
    }

    public function Create(Request $request){
       $validate = $request->validate([
           'title' => 'required|min:3',
           'content' => 'required|min:7',
           'image' => 'required|image|mimes:jpeg,png,jpg'
       ]);
       $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
       // generate a pin based on 2 * 7 digits + a random character
       $pin = mt_rand(100, 999)
       . mt_rand(100, 999)
       . $characters[rand(0, strlen($characters) - 1)];
       // shuffle the result
       $string = str_shuffle($pin);
       $fileName = $string.".jpg";
       $request->file('image')->move(public_path("images"),$fileName);
       $photoUrl = url('images/'.$fileName);
       $data = Posts::insert(['title'=>$request->title, 'content' => $request->content, 'image' => $photoUrl]);
       if($data == true){
           return JsonApiResponse::success('Posted Succesfully');
       }else{
           return JsonApiResponse::error('Error', 108);
       }
    }

    public function Delete(Request $request){
        Posts::where('id', $request->input('id'))->delete();
        return JsonApiResponse::success('Deleted');
    }

    public function getPost(Request $request){
        $data = Posts::where('id', $request->input('id'))->get();
        return JsonApiResponse::success('post', [$data]);
    }

    public function Update(Request $request){
        
        $validate = $request->validate([
               'title' => 'required|min:3',
               'content' => 'required|min:7'
        ]);
        if($request->file('image')!=null){
             $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
             // generate a pin based on 2 * 7 digits + a random character
             $pin = mt_rand(100, 999)
             . mt_rand(100, 999)
             . $characters[rand(0, strlen($characters) - 1)];
             // shuffle the result
             $string = str_shuffle($pin);
             $fileName = $string.".jpg";
             $request->file('image')->move(public_path("images"),$fileName);
             $photoUrl = url('images/'.$fileName);
             $data = Posts::where('id', $request->input('id'))
                          ->update(['title'=> $request->input('title'),
                                    'content'=> $request->input('content'),
                                    'image' => $photoUrl]);
        }else{
             $data = Posts::where('id', $request->input('id'))
                          ->update(['title'=> $request->input('title'),
                                    'content'=> $request->input('content')]);
        }
        if($data == true){
             return JsonApiResponse::success('success');
        }else{
             return JsonApiResponse::error('error', 108);
        }
    }

}
