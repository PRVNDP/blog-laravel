<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Helper\JsonApiResponse;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\admin;
use App\AccessToken;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request){
        $validateData = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:5|confirmed'
        ]);
        $check = User::where('email' ,$request->email)->get();
        if(count($check)>0){
            return JsonApiResponse::success('fail',['email' => 'Account already registered with this email']);
        }
        $validateData['password'] = bcrypt($request->password);
        $user = User::create($validateData);
        if($user){
            return JsonApiResponse::success('success',['id' => $user['id'],'message' => 'Registration Done']);
        }else{
            return JsonApiResponse::error('fail',['message' => 'Something Went Wrong']);
        }
    }


    public function login(Request $request){
       
        $credentials = $request->validate([
             'email' => 'required|email',
             'password' => 'required'
        ]);
        $data = User::where('email', $request->email)->get();
        if(!Auth::attempt($credentials)) {
            return JsonApiResponse::error('Please Enter Correct Details.', 109);
        }else{
            Auth::user()->getPermissionsViaRoles();
            $permissions = collect();
            $roles = collect();

            foreach(Auth::user()->roles as $item){
                foreach($item->permissions as $key){
                    $permissions->push($key['name'])->unique;
                }
                $roles->push($item->name);
            }
        }
        $accessToken = Auth::user()->createToken('authToken')->accessToken;
        if($data[0]['blocked'] == '0'){
           return JsonApiResponse::success('Successfully Logged in..',[[
                   'User_Details' => [
                        'id'=> $data[0]['name'],
                        'access_token' => $accessToken,
                        'roles' => $roles,
                        'permissions' => $permissions
                    ]]]);

        }else{
           return JsonApiResponse::error('Your Account Has Been Blocked.', 110);
        }

    }


    public function logout(){
        $id = Auth::id();
        $data = AccessToken::where('user_id',$id)->update(['revoked'=> '1']);

        if($data == true) {
            return JsonApiResponse::success('success');
        }else{
            return JsonApiResposne::error('Error');
        }
    }

}
