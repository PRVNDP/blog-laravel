<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Helper\JsonApiResponse;
use App\Posts;
use App\User;
use App\Roles;
use Validator;

class UserController extends Controller
{
    public function Index(Request $request){
        $sortBy = $request->sortBy;
        $order = $request->sortDirection;
        $search = $request->search;
        $users = new User();
        if(isset($search)){
            $users = $users->where([['name', '!=', 'admin'], ['name', 'LIKE',"%{$search}%"]])
                           ->orWhere([['name', '!=', 'admin'],['email', 'LIKE', "%{$search}%"]]);
        }
        if(isset($sortBy) && isset($order)){
            $users = $users->orderBy($sortBy, $order);
        }
        $data = $users->where('name', '!=', 'admin')->paginate(10);
        return JsonApiResponse::success('User details',[$data]);
    }

    public function Delete(Request $request){
        $data = User::where('id', $request->id)->delete();
        if($data){
            return JsonApiResponse::success('Success');
        }else{
            return JsonApiResponse::error('Error');
        }
    }

    public function Update(Request $request){
        $vadlidateData = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email'
        ]);
        $data = User::where('id', $request->userId)->update(['name'=> $request->name, 'email' => $request->email]);
        if($data){
            return JsonApiResponse::success('Success');
        }else{
            return JsonApiResponse::error('Error');
        }
    }


    public function Block(Request $request){
        $data = User::where('id', $request->id)->update(['blocked'=>'1']);
        if($data){
            return JsonApiResponse::success('Success');
        }else{
            return JsonApiResponse::error('Error');
        }
    }

    public function UnBlock(Request $request){
        $data = User::where('id', $request->id)->update(['blocked'=> '0']);
        if($data){
            return JsonApiResponse::success('Success');
        }else{
            return JsonApiResponse::error('Error');
        }
    }

    public function BlockedCustomers(Request $request){
        $sortBy = $request->sortBy;
        $order = $request->sortDirection;
        $search = $request->search;
        $user = new User();
        if(isset($search)){
             $user = $user->where([['name', '!=', 'admin'],['blocked','1'],['name', 'LIKE', "%{$search}%"]])
                          ->orWhere([['name', '!=', 'admin'],['blocked','1'],['email', 'LIKE', "%{$search}%"]]);
        }
        if(isset($sortBy) && isset($order)){
             $user = $user->orderBy($sortBy, $order);
        }
        $data = $user->where('blocked', '1')->paginate(10);
        return JsonApiResponse::success('Blocked Users', [$data]);
    }

    public function getuser(Request $request){
        $data = User::where('id', $request->id)->get();
        return JsonApiResponse::success('Success', [$data]);
    }
}
