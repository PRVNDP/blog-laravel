<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::group(["middleware" => "auth:api"], function (){
    Route::get("/logout", "Api\\AuthController@logout");
    Route::post("adduser", "Api\\AuthController@register");

    Route::get("/","HomeController@index");
    Route::post("permissions", "HomeController@permissions");

    Route::post("addpost", "PostController@Create");
    Route::get("getpost", "PostController@getPost");
    Route::post("updatepost", "PostController@Update");
    Route::post("deletepost", "PostController@Delete");
    Route::get("getposts", "PostController@Index");

    Route::get("customers", "UserController@Index");
    Route::get("blockedcustomers", "UserController@BlockedCustomers");
    Route::post("deleteUser", "UserController@Delete");
    Route::post("updateUser", "UserController@Update");
    Route::post("blockUser", "UserController@Block");
    Route::post("unblockUser", "UserController@UnBlock");
    Route::post("getuser", "UserController@getuser");
});



Route::post("/register", "Api\\AuthController@register");
Route::post("/login", "Api\\AuthController@login");
Route::post("/adminlogin", "Api\\AuthController@login");
				